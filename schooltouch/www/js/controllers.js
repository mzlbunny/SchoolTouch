angular.module('starter.controllers', [])

/********************校园主页******************************/
 
//首页主页 
.controller('HomeCtrl', function($scope,HomeService,IMG_URL,$ionicSlideBoxDelegate) {
  //初始化变量
  $scope.activitys = [];
  $scope.articles = [];
  $scope.IMG_URL = IMG_URL;
  //获取首页列表
  HomeService.AllList().then(function(res){
    console.log(res);
    $ionicSlideBoxDelegate.update();
    var actives  = res.data.actives;
    var articles = res.data.articles;
    $scope.news  = res.data.news;
    //获取活动列表
    for(var i=0;i<actives.length;i++){
        var id = actives[i].id;
        //获取活动的详情
        HomeService.Artivity(id).then(function(res){
          var year = res.data.datas.createtime.substring(0,4);
          var month = res.data.datas.createtime.substring(5,7);
          var day = res.data.datas.createtime.substring(8,10);
          //活动倒计时
          var startTime = (new Date(year,month,day,7+8,00)).getTime();
          function CountDown(startTime){
            var now = (new Date()).getTime(); 
            var a  = parseInt(startTime-now);
            if(a<0) return false;
            var countdown = formatDuring(a);
            return countdown;
          }
          function formatDuring(mss) {
            var days = parseInt(mss / (1000 * 60 * 60 * 24));
            var hours = parseInt((mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = parseInt((mss % (1000 * 60 * 60)) / (1000 * 60));
            return days + "天" + hours + "小时" + minutes + "分";
          }
          function forgetStartTime(mss){
            var year = parseInt(mss.getFullYear());
            var month = parseInt(mss.getMonth());
            var days = parseInt(mss.getDay());
            return year + "/" + month + "/" + days;
          }
          res.data.datas.createtime = CountDown(startTime)?CountDown(startTime):'';
          //活动表数组
          $scope.activitys.push(res.data.datas);

        });
    }
    //获取文章列表
    for(var i=0;i<articles.length;i++){
      var id = articles[i].id;
       HomeService.Articles(id).then(function(res){
          console.log(res);
          if(res.data.datas.images){
            var imgs = res.data.datas.images.split(',');
            res.data.datas.images = imgs[0];
          }
          $scope.articles.push(res.data.datas);
       });
    }

  });

})

/********************活动广场******************************/

//活动广场主页
.controller('ActivityCtrl', function($scope,HomeService,IMG_URL) {
  //初始化变量
  $scope.activitys = [];
  $scope.IMG_URL = IMG_URL;
 HomeService.AllList().then(function(res){
    console.log(res);
    var actives  = res.data.actives;
    //获取活动列表
    for(var i=0;i<actives.length;i++){
        var id = actives[i].id;
        //获取活动的详情
        HomeService.Artivity(id).then(function(res){
          var year = res.data.datas.createtime.substring(0,4);
          var month = res.data.datas.createtime.substring(5,7);
          var day = res.data.datas.createtime.substring(8,10);
          //活动倒计时
          var startTime = (new Date(year,month,day,7+8,00)).getTime();
          function CountDown(startTime){
            var now = (new Date()).getTime(); 
            var a  = parseInt(startTime-now);
            if(a<0) return false;
            var countdown = formatDuring(a);
            return countdown;
          }
          function formatDuring(mss) {
            var days = parseInt(mss / (1000 * 60 * 60 * 24));
            var hours = parseInt((mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = parseInt((mss % (1000 * 60 * 60)) / (1000 * 60));
            return days + "天" + hours + "小时" + minutes + "分";
          }
          function forgetStartTime(mss){
            var year = parseInt(mss.getFullYear());
            var month = parseInt(mss.getMonth());
            var days = parseInt(mss.getDay());
            return year + "/" + month + "/" + days;
          }
          res.data.datas.createtime = CountDown(startTime)?CountDown(startTime):'';
          //活动表数组
          $scope.activitys.push(res.data.datas);

        });
    }
  });
})
//活动详情
.controller('ActivityDetailCtrl', function($scope,$stateParams,ActivityDetailService,IMG_URL,showloaing) {
  //变量初始化
  $scope.IMG_URL = IMG_URL;
  //获取活动的详情
  ActivityDetailService.GetActivityDetail($stateParams.id).then(function(res){
    console.log(res);
    $scope.activity = res.data.datas;

  })
  $('.activity_getin').click(function() {//参与活动
    ActivityDetailService.JoinActivity($stateParams.id).then(function(res){
      console.log(res);
      showloaing.showLoading(res.data.message,1000);
    });
  });
})
//活动发布
.controller('ActivityIssueCtrl', function($scope,ActivityIssueService,showloaing,$state) {
  //发布新的活动
  $('.header_plus').click(function(){
     var title = $('#ActivityTitle').val();
     var address = $('#Activityposition').val();
     var content  = $('#ActivityContent').val();
     console.log(content);
    ActivityIssueService.GetActivitys(title,address,content).then(function(res){
      console.log(res);
      showloaing.showLoading('发布成功',1000);
      $state.go('account-activity');
    });
  });
  //
  var StartTime = new datePicker();
  var EndTime = new datePicker();
    StartTime.init({
      'trigger': '#startTime', /*按钮选择器，用于触发弹出插件*/
      'type': 'date',/*模式：date日期；datetime日期时间；time时间；ym年月；*/
      'minDate':'2017-4-22',/*最小日期*/
      'maxDate':'2100-12-31',/*最大日期*/
      'onSubmit':function(){/*确认时触发事件*/
          
      },
      'onClose':function(){/*取消时触发事件*/
      }
  });
    EndTime.init({
      'trigger': '#endTime', /*按钮选择器，用于触发弹出插件*/
      'type': 'date',/*模式：date日期；datetime日期时间；time时间；ym年月；*/
      'minDate':'2017-4-22',/*最小日期*/
      'maxDate':'2100-12-31',/*最大日期*/
      'onSubmit':function(){/*确认时触发事件*/
          
      },
      'onClose':function(){/*取消时触发事件*/
      }
  });
})

/********************二手广场******************************/

//二手市场主页 
.controller('UsedgoodsCtrl', function($scope,UsedService,IMG_URL) {
  $scope.$on('$ionicView.beforeEnter',function(){
  //初始化变量
  $scope.IMG_URL = IMG_URL;
  //获取所有的二手商品
  UsedService.Getgoods().then(function(res) {
    console.log(res.data);
    //瀑布流商品展示初始化
    $scope.goods_item = res.data;
    var goodlist = document.getElementsByClassName('goodlist')[0];
    falls(goodlist,$scope.goods_item);
  })


  //商品列表位置操作
  function setLiplace(arr,i,height_num){
      var space = new Array();
      var turn_space = new Array();
      space[0] = space[1] = 0;//top & left
      turn_space[0] = turn_space[1] = 0;//return
      if(i%2==1){
        turn_space[0] = arr[2]*0.5+5;//返回的left
        turn_space[1] = height_num[1];//返回的top
        space[1] = arr[1];//当前操作li高度
        height_num[1] += space[1]+20;//列的高度
      }else{
        turn_space[0] = 5;//返回的left
        turn_space[1] = height_num[0];//返回的top
        space[1] = arr[1];//当前操作li高度
        height_num[0] += space[1]+20;//列的高度
      }

      return turn_space;
  }  

  //瀑布流函数
  function falls(obj,data){
    var tag = obj;
    var data = data;
    var length = data.length;
    var widows_width = document.body.clientWidth;
    var height_num = new Array();
    height_num[1] = height_num [0] = 0;
    for(var i=0;i<length;i++){
      var arr = new Array();
      //创建li与img标签
      var li = document.createElement('li');
      var img = document.createElement('img');
      var p1 = document.createElement('p');
      var p2 = document.createElement('p');
      var imgbox = document.createElement('div');
      var head = document.createElement('img');
      var nickname = document.createElement('div');
      var link  = document.createElement('a');
      link.href = '#/used-detail/'+ data[i].id; 
      link.style.color = '#666';
      imgbox.className = 'tab_usedgoods_owerhead';
      head.src =  $scope.IMG_URL+data[i].thumb;
      imgbox.appendChild(head);
      nickname.className = 'tab_usedgoods_owernickname';
      nickname.innerHTML = data[i].nick;
      p1.innerHTML = data[i].name;
      p2.innerHTML = '￥ '+data[i].price;
      img.src = $scope.IMG_URL+data[i].images;
      li.style.position  = 'absolute';
      //设置图片位置
      link.appendChild(img);
      link.appendChild(p1);
      link.appendChild(p2);
      link.appendChild(imgbox);
      link.appendChild(nickname);
      li.appendChild(link);
      tag.appendChild(li);
      arr[0] = li.scrollWidth;
      arr[1] = li.scrollHeight;
      arr[2] = widows_width;
      var space = setLiplace(arr,i,height_num);
      li.style.top = space[1] + 'px';
      li.style.left = space[0] + 'px';
    }

  }
})
})
//二手商品信息详情
.controller('UsedDetailCtrl', function($scope, $stateParams,UsedService,IMG_URL) {
    //变量初始化
    $scope.IMG_URL = IMG_URL;
    //获取商品的详细信息
    UsedService.Getgoods().then(function(res){
      console.log(res.data);
      for(var i=0;i<res.data.length;i++){
        if($stateParams.id == res.data[i].id){
          $scope.goods = res.data[i];
        }
      }
    });
})
//二手商品信息发布
.controller('UsedIssueCtrl', function($scope,IMG_URL,BASE_URL,showloaing,$state) {
    //变量初始化
    $scope.IMG_URL = IMG_URL;
    var formdata = new FormData();
    var http = new XMLHttpRequest();
    //富文本编辑器
    function initialize(){
      $('#content').artEditor({
        imgTar: '#imageUpload',
        limitSize: 5,   // 兆
        showServer: false,
        uploadUrl: '',
        data: {},
        uploadField: 'image',
        placeholader: '<p>请输入商品的描述</p>',
        validHtml: ["br"],
      });
    }
    initialize();
    $('.header_plus').click(function() {
      initialize();
      var value = $('#content').getValue();
      var title = $('#title').val();
      var price = $('#price').val();
      var phone = $('#phone').val();
      formdata.append('name',title);
      formdata.append('price',price);
      formdata.append('description',value);
      formdata.append('mobile',phone);
      formdata.append('account',localStorage.account);
      formdata.append('checkkey',localStorage.checkkey);
      formdata.append('token',localStorage.token); 
      http.open('post',BASE_URL+'/market/add',true);
      http.onload = function (data) {
          (http.readyState == 4 || http.status == 200) ? SedSuccess() : err();
      }
      http.onerror = function () { err() };
      http.send(formdata);
    });
    function SedSuccess(){
      showloaing.showLoading('发布成功',1000);
      $state.go('account-used');
    }
    //封面上传
    function getImgUrl(file){//获取图片的url
      var url;
      var Img = new FileReader();//读取文件并转换成base64字节流
      Img.onload = function(){

      }
      Img.readAsDataURL(file);
      url = window.URL.createObjectURL(file);
      return url;
    }
    $('#thumbUpload').change(function() {
      var _this = $(this)[0].files[0];
      $('.upthumb-box').css({
        'background':'url(' + getImgUrl(_this) + ')',
        'background-size':'100% 100%'
      });
      formdata.append('images',_this);
    });
})

/********************校友列表******************************/

//好友列表主页 
.controller('FriendsCtrl', function($scope,IMG_URL,FriendsService,$state) {
  //初始化变量
  $scope.IMG_URL = IMG_URL;
  //获取好友列表
  FriendsService.MyFriends().then(function(res){
      console.log(res);
      $scope.Friends = res.data.datas;
  });
  $scope.$on("ngRepeatFinished", function (repeatFinishedEvent, element){
    initials();
  });
  //搜索好友跳转
  $('.search_friends').focus(function() {
    $state.go('friends-search');
  });
  //通讯录排序
 function initials() {//排序
    var SortList=$(".friends_item_name");
    var SortBox=$(".friends_items_box");
    SortList.sort(asc_sort).appendTo('.friends_items_box');//按首字母排序
    function asc_sort(a, b) {
        return makePy($(b).find('.name_num').text().charAt(0))[0].toUpperCase() < makePy($(a).find('.name_num').text().charAt(0))[0].toUpperCase() ? 1 : -1;
    }

    var initials = [];
    var num=0;
    SortList.each(function(i) {
        var initial = makePy($(this).find('.name_num').text().charAt(0))[0].toUpperCase();
        if(initial>='A'&&initial<='Z'){
            if (initials.indexOf(initial) === -1)
                initials.push(initial);
        }else{
            num++;
        }
        
    });

    $.each(initials, function(index, value) {//添加首字母标签
        //SortBox.append('<div class="sort_letter" id="'+ value +'">' + value + '</div>');
        SortBox.append('<li class="friends_item_title" id="'+value+'">' +value + '</li>');
    });
    if(num!=0){
      SortBox.append('<li class="friends_item_title" id="default">#</li>')
      //SortBox.append('<div class="sort_letter" id="default">#</div>');
    }

    for (var i =0;i<SortList.length;i++) {//插入到对应的首字母后面
        var letter=makePy(SortList.eq(i).find('.name_num').text().charAt(0))[0].toUpperCase();
        switch(letter){
            case "A":
                $('#A').after(SortList.eq(i));
                break;
            case "B":
                $('#B').after(SortList.eq(i));
                break;
            case "C":
                $('#C').after(SortList.eq(i));
                break;
            case "D":
                $('#D').after(SortList.eq(i));
                break;
            case "E":
                $('#E').after(SortList.eq(i));
                break;
            case "F":
                $('#F').after(SortList.eq(i));
                break;
            case "G":
                $('#G').after(SortList.eq(i));
                break;
            case "H":
                $('#H').after(SortList.eq(i));
                break;
            case "I":
                $('#I').after(SortList.eq(i));
                break;
            case "J":
                $('#J').after(SortList.eq(i));
                break;
            case "K":
                $('#K').after(SortList.eq(i));
                break;
            case "L":
                $('#L').after(SortList.eq(i));
                break;
            case "M":
                $('#M').after(SortList.eq(i));
                break;
            case "N":
                $('#N').after(SortList.eq(i));
                break;
            case "O":
                $('#O').after(SortList.eq(i));
                break;
            case "P":
                $('#P').after(SortList.eq(i));
                break;
            case "Q":
                $('#Q').after(SortList.eq(i));
                break;
            case "R":
                $('#R').after(SortList.eq(i));
                break;
            case "S":
                $('#S').after(SortList.eq(i));
                break;
            case "T":
                $('#T').after(SortList.eq(i));
                break;
            case "U":
                $('#U').after(SortList.eq(i));
                break;
            case "V":
                $('#V').after(SortList.eq(i));
                break;
            case "W":
                $('#W').after(SortList.eq(i));
                break;
            case "X":
                $('#X').after(SortList.eq(i));
                break;
            case "Y":
                $('#Y').after(SortList.eq(i));
                break;
            case "Z":
                $('#Z').after(SortList.eq(i));
                break;
            default:
                $('#default').after(SortList.eq(i));
                break;
        }
    };
}

})
//好友资料详情
.controller('FriendsDetailCtrl', function($scope,FriendDetialService,showloaing,$stateParams,FriendSearchService,FriendsService,IMG_URL) {
  //变量初始化
  $scope.IMG_URL = IMG_URL;
  $scope.friendDetail = '';
  //获取我的好友列表
  FriendsService.MyFriends().then(function(res){
    console.log(res);
    for(var i=0;i<res.data.datas.length;i++){
      if($stateParams.name == res.data.datas[i].nick){
        $('.friends_detail_items').css('display','none');
      }
    }
  });
  //获取好友详情
  FriendSearchService.SearchFriends($stateParams.name).then(function(res){
      console.log(res);
      for(var i=0;i<res.data.datas.length;i++){
        if($stateParams.name == res.data.datas[i].nick){
          $scope.friendDetail = res.data.datas[i];
        }
      }
      
  });
  //发送好友请求
  $('#addfriend').click(function() {
      var message = '交个朋友吧！';
      FriendDetialService.BeMyFriend($scope.friendDetail.id,message).then(function(res){
        console.log(res);
        if(res.data.message == '数据获取成功'){
          showloaing.showLoading('已经发送好友请求了哦!',1000);
          $('.friends_detail_items').css('display','none');
        }else {
          showloaing.showLoading('发送请求失败!',1000);
        }
      });
  });

})

//查找好友
.controller('FriendsSearchCtrl', function($scope,FriendSearchService,IMG_URL) {
  //变量初始化
  $scope.IMG_URL = IMG_URL;
  //按照关键词搜索好友
   $('.subimtSearch').click(function() {
      var SearchKey = $('.SearchKey').val();
      console.log(SearchKey);
      FriendSearchService.SearchFriends(SearchKey).then(function(res){
        console.log(res);
        $scope.Friends = res.data.datas;
      })
   });
})

//好友聊天对话框
.controller('FriendsDialogCtrl', function($scope,FriendDialogService,IMG_URL,showloaing) {
  //变量初始化
  $scope.IMG_URL = IMG_URL;
  $scope.leavesList = [];
  $scope.activityList = [];
  $scope.FriendsList = [];
  //获取评论的信息 
  FriendDialogService.MyArticles().then(function(res){
    for(var i=0;i<res.data.datas.length;i++){
      if(res.data.datas[i].leaves.length>0){
        for(var j=0;j<res.data.datas[i].leaves.length;j++){
          var leaves = {};
          leaves.leaves = res.data.datas[i].leaves[j].leaves;
          leaves.nick = res.data.datas[i].leaves[j].nick;
          leaves.thumb = res.data.datas[i].leaves[j].thumb;
          leaves.time = res.data.datas[i].leaves[j].time.substring(0,11);
          leaves.articleTitle = res.data.datas[i].title;
          leaves.articleContent = res.data.datas[i].content;
          $scope.leavesList.push(leaves);
        }
      }
    }
  });
  //获取我发布的活动列表
  FriendDialogService.MyArtivity().then(function(res){
    $scope.MyArtivity = res.data.datas;
    //获取参与我活动的用户列表
     FriendDialogService.MyArtivityUser().then(function(res){
      if(res.data.datas.length!=0){
        for(var j=0;j<res.data.datas.length;j++){
          var ActivityUser = {};
          ActivityUser.nick = res.data.datas[j].nick;
          ActivityUser.thumb = res.data.datas[j].thumb;
          ActivityUser.title = res.data.datas[j].title;
          ActivityUser.status = res.data.datas[j].status;
          ActivityUser.id = res.data.datas[j].id;
          for(var i=0;i<$scope.MyArtivity.length;i++){
             if(res.data.datas[j].title==$scope.MyArtivity[i].title){
              ActivityUser.content = $scope.MyArtivity[i].content;
             }
          }
          $scope.activityList.push(ActivityUser);
        }
      }
     });
  })
  //同意或者拒绝用户的活动申请
  $scope.confirm = function(status,reqeustid){
    FriendDialogService.ConfirmActivity(status,reqeustid).then(function(res){
      if(status==1){
        showloaing.showLoading('欣然接受',1000);
      }else{
        showloaing.showLoading('无情拒绝',1000);
      }
      
    });
  }
  //获取好友申请列表
  FriendDialogService.FriendsList().then(function(res){
    console.log(res);
    $scope.FriendsList = res.data.datas;
  });
  //好友申请确认
  $scope.ConfirFriends = function(status,reqeustid){
      console.log(status);
      FriendDialogService.FriendsConfirm(status,reqeustid).then(function(res){
        console.log(res);
        if(status==1){
          showloaing.showLoading('欣然接受',1000);
        }else{
          showloaing.showLoading('无情拒绝',1000);
        }
      })
  }

})
//聊天对话框
.controller('DialogDetailCtrl', function($scope) {
  
})

/********************文章板块******************************/

//文章详情
.controller('ArticleDetailCtrl', function($scope, $stateParams,ActicleDetailService,IMG_URL,showloaing) {
    //变量初始化
    $scope.IMG_URL = IMG_URL;
    //载入文章和评论
    LoadArticle();
    function LoadArticle(){//载入文章和评论好书
      ActicleDetailService.GetActicleDetail($stateParams.id).then(function(res){
          console.log(res);
          res.data.datas.time = res.data.datas.time.substring(0,10); 
          for(var i=0;i<res.data.datas.leaves.length;i++){
            res.data.datas.leaves[i].time = res.data.datas.leaves[i].time.substring(0,10);
          }
          $scope.article = res.data.datas;
          // $('.article_detail_content').html(res.data.datas.content);
          $scope.content_imgs = res.data.datas.images.split(',');
      });
    }
    //添加新的评论
    $('.ion-android-send').click(function(){
        var leave = $('#NewComment').val();//获取用户评论的内容
        if(!leave){
            showloaing.showLoading('请先说点什么吧!',1000);
        }else {
            showloaing.showLoading('评论成功!',1000);
            ActicleDetailService.NewLeaves($stateParams.id,leave).then(function(res){
              //载入新的评论
              LoadArticle();
            });
        }
    })
})
//文章发布
.controller('ArticleIssueCtrl', function($scope,$rootScope,$state,ActicleIssueService,BASE_URL) {
    //变量初始化
    var ImgFile = '';
    var formdata = new FormData();      //以下为像后台提交图片数据
    var http = new XMLHttpRequest();
    //封面上传
    function getImgUrl(file){//获取图片的url
      var url;
      var Img = new FileReader();//读取文件并转换成base64字节流
      Img.onload = function(e){
        // console.log(e.target.result);
      }
      Img.readAsDataURL(file);
      url = window.URL.createObjectURL(file);
      return url;
    }
    $('#thumbUpload').change(function() {
      var _this = $(this)[0].files[0];
      $('.upthumb-box').css({
        'background':'url(' + getImgUrl(_this) + ')',
        'background-size':'100% 100%'
      });
      formdata.append('images0',_this);
    });
    //富文本编辑器
    function initialize(){
      $('#content').artEditor({
        imgTar: '#imageUpload',
        limitSize: 5,   // 兆
        showServer: false,
        uploadUrl: '',
        data: {},
        uploadField: 'image',
        placeholader: '<p>请输入文章正文内容</p>',
        validHtml: ["br"]
      });
    }
    initialize();
    $('.header_plus').click(function() {
      initialize(); 
      var value = $('#content').getValue();
      var title = $('#title').val();  
      formdata.append('title',title);
      formdata.append('images',1);
      formdata.append('content',value);
      formdata.append('account',localStorage.account);
      formdata.append('checkkey',localStorage.checkkey);
      formdata.append('token',localStorage.token);
      http.open('post',BASE_URL+'/member/article/add',true);
      http.onload = function (data) {
          (http.readyState == 4 || http.status == 200) ? console.log(http.responseText) : err();
      }
      http.onerror = function () { err() };
      http.send(formdata);
      $state.go('account-article');
    });

})
//通知详情
.controller('NewsDetailCtrl', function($scope, $stateParams,NewsDetailService,IMG_URL) {
    //变量初始化
    $scope.IMG_URL = IMG_URL;
    //载入新闻内容
    NewsDetailService.GetNewsDetail().then(function(res){
      console.log(res);
      for(var i=0;i<res.data.news.length;i++){
        if($stateParams.id == res.data.news[i].id){
          $scope.news = res.data.news[i];
        }
      }
    console.log($scope.news);
    })

})
/********************登入注册******************************/
//登入
.controller('LoginCtrl', function($scope,LoginService,$state,showloaing) {
  //提交登入账号密码
  $('.header_plus').click(function(event) {
    $scope.account = $('#account').val();
    $scope.password =$('#password').val();
     LoginService.Login($scope.account,$scope.password).then(function(res){
        console.log(res);
        localStorage.token = res.data.token;
        localStorage.checkkey = res.data.checkkey;
        localStorage.account = $scope.account;
        console.log(localStorage.token);       
        if(res.data.result==true){
          showloaing.showLoading('登入成功',1000);
          $state.go('tab.account');
        }
        else {
          showloaing.showLoading(res.data.message,1000);
        }
     }); 
  });

})
//注册
.controller('SignCtrl', function($scope,SignUpService,showloaing,$state) {
  //提交注册账号密码
  $('.header_plus').click(function() {
    $scope.account = $('#account').val();
    $scope.password =$('#password').val();
     SignUpService.SignUp($scope.account,$scope.password).then(function(res){
        console.log(res);
        localStorage.token = res.data.token;
        localStorage.checkkey = res.data.checkkey;
        localStorage.account = $scope.account;
        if(res.data.result==true){
          showloaing.showLoading('登入成功',1000);
          $state.go('tab.account');
        }
        else {
          showloaing.showLoading(res.data.message,1000);
        }
     }); 
  });
})

/********************个人中心******************************/

//个人中心主页
.controller('AccountCtrl', function($scope,AccountService,showloaing,IMG_URL,$state) {
  $scope.IMG_URL = IMG_URL;
  //检测是否登入
  $scope.loginstate = localStorage.token;//已登入

  //退出登入
   $scope.signout = function() {
    localStorage.account = '';
    localStorage.token ='';
    localStorage.checkkey= '';
    showloaing.showLoading('退出登入',1000);
    $state.go('tab.home');
  }
  //获取用户信息
  AccountService.Getuserifo().then(function(res){
    console.log(res);
    $scope.userifo = res.data.datas;
  })

})
//个人资料详情
.controller('AccountDetailCtrl', function($scope,AccountDetailService,IMG_URL,BASE_URL,showloaing) {
  //初始化变量
  $scope.IMG_URL = IMG_URL;
  var formdata = new FormData();
  var http =  new XMLHttpRequest();
  $scope.Img_base = './img/goods2.jpg';
  if(localStorage.Img_base)$scope.Img_base = localStorage.Img_base;
  //获取用户信息
  AccountDetailService.Getuserifo().then(function(res){
    console.log(res);
    $scope.userifo = res.data.datas;
  })

  //封面上传
  function getImgUrl(file){//获取图片的url
    var url;
    var Img = new FileReader();//读取文件并转换成base64字节流
    Img.onload = function(){

    }
    Img.readAsDataURL(file);
    url = window.URL.createObjectURL(file);
    return url;
  } 
  function ImgBox(file){//设置本地的背景资料卡
    var Img = new FileReader();//读取文件并转换成base64字节流
    Img.onload = function(e){
      localStorage.Img_base = e.target.result;
    }
    Img.readAsDataURL(file);
  }
  $('#USerHead').change(function() {//修改头像
    var _this = $(this)[0].files[0];
    $('.acconut_detail_head_owerhead img').attr({
      src: getImgUrl(_this)
    });
    formdata.append('image',_this);
  });
  $('#USerThumb').change(function() { //修改背景图片
    var _this = $(this)[0].files[0];
    ImgBox(_this);
    $('.account_detail_head_imgbox img').attr({
      src: getImgUrl(_this)
    });
  });
  //确认更新信息
  $('.header_plus').click(function() {
      //更新用户信息
      AccountDetailService.UpdataIfo($scope.userifo).then(function(res){
        console.log(res);
      });
      //更新用户头像
      formdata.append('account',localStorage.account);
      formdata.append('checkkey',localStorage.checkkey);
      formdata.append('token',localStorage.token);
      http.open('post',BASE_URL+'/member/thumb/update',true);
      http.onload = function (data) {
          (http.readyState == 4 || http.status == 200) ? showloaing.showLoading('修改成功',1000) : err();
      }
      http.onerror = function () { err() };
      http.send(formdata);
    });
})
//我的活动
.controller('AccountActivityCtrl', function($scope,AccountActivityService,HomeService,IMG_URL) {
  //变量初始化
  $scope.IMG_URL = IMG_URL;
  $scope.IssueActivitys = [];
  $scope.JoinActivitys = [];
  //获取我发布的活动列表
  AccountActivityService.MyArtivity().then(function(res){
    console.log(res.data);
    var actives = res.data.datas; 
    for(var i=0;i<actives.length;i++){
            var id = actives[i].id;
            //获取活动的详情
            HomeService.Artivity(id).then(function(res){
              var year = res.data.datas.createtime.substring(0,4);
              var month = res.data.datas.createtime.substring(5,7);
              var day = res.data.datas.createtime.substring(8,10);
              //活动倒计时
              var startTime = (new Date(year,month,day,7+8,00)).getTime();
              function CountDown(startTime){
                var now = (new Date()).getTime(); 
                var a  = parseInt(startTime-now);
                if(a<0) return false;
                var countdown = formatDuring(a);
                return countdown;
              }
              function formatDuring(mss) {
                var days = parseInt(mss / (1000 * 60 * 60 * 24));
                var hours = parseInt((mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = parseInt((mss % (1000 * 60 * 60)) / (1000 * 60));
                return days + "天" + hours + "小时" + minutes + "分";
              }
              function forgetStartTime(mss){
                var year = parseInt(mss.getFullYear());
                var month = parseInt(mss.getMonth());
                var days = parseInt(mss.getDay());
                return year + "/" + month + "/" + days;
              }
              res.data.datas.createtime = CountDown(startTime)?CountDown(startTime):'';
              //活动表数组
              $scope.IssueActivitys.push(res.data.datas);
            });
        }
  });
  //获取我参与的活动列表
  AccountActivityService.JoinActivity().then(function(res){
    console.log(res.data);
    var actives = res.data.datas; 
    for(var i=0;i<actives.length;i++){
            var id = actives[i].id;
            //获取活动的详情
            HomeService.Artivity(id).then(function(res){
              var year = res.data.datas.createtime.substring(0,4);
              var month = res.data.datas.createtime.substring(5,7);
              var day = res.data.datas.createtime.substring(8,10);
              //活动倒计时
              var startTime = (new Date(year,month,day,7+8,00)).getTime();
              function CountDown(startTime){
                var now = (new Date()).getTime(); 
                var a  = parseInt(startTime-now);
                if(a<0) return false;
                var countdown = formatDuring(a);
                return countdown;
              }
              function formatDuring(mss) {
                var days = parseInt(mss / (1000 * 60 * 60 * 24));
                var hours = parseInt((mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = parseInt((mss % (1000 * 60 * 60)) / (1000 * 60));
                return days + "天" + hours + "小时" + minutes + "分";
              }
              function forgetStartTime(mss){
                var year = parseInt(mss.getFullYear());
                var month = parseInt(mss.getMonth());
                var days = parseInt(mss.getDay());
                return year + "/" + month + "/" + days;
              }
              res.data.datas.createtime = CountDown(startTime)?CountDown(startTime):'';
              //活动表数组
              $scope.JoinActivitys.push(res.data.datas);
            });
        }
  });



})

//我的二手商品
.controller('AccountUsedCtrl', function($scope,AccountUsedService,IMG_URL) {
  //变量初始化
  $scope.IMG_URL = IMG_URL;
  //获取我的二手商品列表
  AccountUsedService.MyUsed(localStorage.account,localStorage.token,localStorage.checkkey).then(function(res){
    console.log(res.data);
    $scope.goods = res.data.datas; 
  });

})


//我的文章
.controller('AccountArticleCtrl', function($scope,AccountArticleService,IMG_URL) {
  //初始化变量
  $scope.IMG_URL = IMG_URL;
  //获取文章列表
  AccountArticleService.MyArticles().then(function(res){
    console.log(res);
    $scope.article = res.data.datas;
  })


})

//我的定位
.controller('MyPositionCtrl', function($scope){
 // 百度地图API功能
  var map = new BMap.Map("allmap");
  map.centerAndZoom(new BMap.Point(115.86071,28.67619),16);
  map.enableScrollWheelZoom(true);
  var geolocation = new BMap.Geolocation();
  geolocation.getCurrentPosition(function(r){
    if(this.getStatus() == BMAP_STATUS_SUCCESS){
      // var mk = new BMap.Marker(r.point);
      // map.addOverlay(mk);
      // map.panTo(r.point);
      // alert('您的位置：'+r.point.lng+','+r.point.lat);
      // 创建小狐狸
      console.log(r.point);
      map.clearOverlays();
      var pt = new BMap.Point(r.point.lng,r.point.lat);
      var myIcon = new BMap.Icon("http://developer.baidu.com/map/jsdemo/img/fox.gif", new BMap.Size(200,200));
      var marker2 = new BMap.Marker(pt,{icon:myIcon});  // 创建标注
      map.addOverlay(marker2); 
      map.panTo(pt);
    }
    else {
      alert('failed'+this.getStatus());
    }        
  },{enableHighAccuracy: true});
  // 用经纬度设置地图中心点
  // function theLocation(){
  //   // if(device.baiduLat&&device.baiduLng){
  //     map.clearOverlays();
  //     var point = new BMap.Point(115.86071,28.67619);
  //     var marker = new BMap.Marker(point);  // 创建标注
  //     map.addOverlay(marker);              // 将标注添加到地图中
  //     map.panTo(point);
  //     // 创建小狐狸
  //     // var pt = new BMap.Point(baidulng, baidulat);
  //     // var myIcon = new BMap.Icon("http://developer.baidu.com/map/jsdemo/img/fox.gif", new BMap.Size(300,157));
  //     // var marker2 = new BMap.Marker(pt,{icon:myIcon});  // 创建标注
  //     // map.addOverlay(marker2);              // 将标注添加到地图中
  //   // }
  // }
  // theLocation();
})

//控制器结束分号
;
