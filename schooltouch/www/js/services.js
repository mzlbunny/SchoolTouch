//页面的各种服务
angular.module('starter.services', [])
//定义常量
.constant('Self_URL', 'http://127.0.0.1:8080')
.constant('BASE_URL', 'http://school.cool1024.com')
.constant('IMG_URL',  'http://school.cool1024.com')
//点击涟漪效果
.factory("createEffect", function ($animate,$compile,$rootScope) {
  var x;
  var y;
  var span=angular.element("<span class='animateSpan'></span>");
  var scopeNew=$rootScope.$new(true);
  var spanEffect=$compile(span)(scopeNew);
  return{
    addEffect: function (obj) {
      obj.on("click", function (e) {
      	console.log(1);
        var e=e||event;
        obj.empty();
        $animate.enter(spanEffect,obj);
        x= e.pageX-this.offsetLeft-parseInt($(obj).find("span").css("width"))/2;
        y= e.pageY-this.offsetTop-parseInt($(obj).find("span").css("width"))/2;
        $(obj).find("span").css("left",x);
        $(obj).find("span").css("top",y);
        obj.find("span").addClass("animate");
      })
    }
  }
})

.service('showloaing',  function($ionicLoading){
	  /*气泡输入框*/
	  this.showLoading =function(str,duration) {
	    $ionicLoading.show({ template: str, noBackdrop: true, duration: duration});
	  }
})
 
//首页的服务
.factory('HomeService', function($http,BASE_URL) { 
	return {
		AllList:function(){//获取文章列表
			return $http.get(BASE_URL + '/home/datas');
		},
		Artivity:function(id){//获取活动详情
			return $http.get(BASE_URL + '/member/active/info?id='+id);
		},
		Articles:function(id){//获取文章详情
			return $http.get(BASE_URL + '/member/article/info?articleid='+id);
		}
	}
})
/*******************二手商品模块********************/
//二手市场
.factory('UsedService', function($http,BASE_URL) { 
	return {
		Getgoods:function(){//获取所有的商品列表
			return $http.post(BASE_URL + '/market/list');
		}
	}
})
//二手商品详情
.factory('UsedDetailService', function($http,BASE_URL) { 
	return {
		GetgoodsDetail:function(id){//获取所有的商品列表
			return $http.post(BASE_URL + '/market/info',{id:id});
		}
	}
})
/*******************活动广场模块********************/
//活动广场
.factory('ActivityService', function($http,BASE_URL) { 
	return {
		GetActivitys:function(){//获取所有的活动列表
			return $http.get(BASE_URL + '/activity/list');
		}
	}
})
//活动详情
.factory('ActivityDetailService', function($http,BASE_URL) { 
	return {
		GetActivityDetail:function(id){//获取活动的详情
			return $http.get(BASE_URL + '/member/active/info?id='+id);
		}, 
		JoinActivity:function(id){//参与活动
			return $http.post(BASE_URL + '/member/active/request',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey,activeid:id});
		}
	}
})
//活动发布
.factory('ActivityIssueService', function($http,BASE_URL) { 
	return {
		GetActivitys:function(title,address,content){
			return $http.post(BASE_URL + '/member/active/add',{address:address,content:content,title:title,account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		}
	}
})
/*******************文章模块********************/
//文章详情
.factory('ActicleDetailService', function($http,BASE_URL) { 
	return {
		GetActicleDetail:function(id){//获取文章的详情
			return $http.get(BASE_URL + '/member/article/info?articleid='+id);
		},
		NewLeaves:function(id,leaves){
			return $http.post(BASE_URL + '/member/article/leaves',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey,articleid:id,leaves:leaves});
		}
	}
})
//文章发布
.factory('ActicleIssueService', function($http,BASE_URL) { 
	return {
		IssueActicle:function(img,title,content){//
			return $http.post(BASE_URL + '/member/article/add',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey,title:title,content:content,images:img});
		}
	}
})
//通知详情
.factory('NewsDetailService', function($http,BASE_URL) { 
	return {
		GetNewsDetail:function(){//获取通知内容
			return $http.get(BASE_URL + '/home/datas');
		}
	}
})
//登入、注册页面服务
.factory('LoginService', function($http,BASE_URL) { 
	return {
		Login:function(account,password){
			return $http.post(BASE_URL + '/auth/login',{account:account,password:password});
		}
	}
})
.factory('SignUpService', function($http,BASE_URL) { 
	return {
		SignUp:function(account,password) {
			return $http.post(BASE_URL + '/auth/signup',{account:account,password:password});
		}
	}
})
//个人中心的服务
.factory('AccountService', function($http,BASE_URL){
	return {
		Getuserifo:function(){
			return $http.post(BASE_URL + '/member/info',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		}
	}
})
//资料详情
.factory('AccountDetailService', function($http,BASE_URL){
	return {
		Getuserifo:function(){
			return $http.post(BASE_URL + '/member/info',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		},
		UpdataIfo:function(userifo){
			return $http.post(BASE_URL + '/member/info/update',{class:userifo.class,email:userifo.email,description:userifo.description,nick:userifo.nick,account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		}
	}
})
//我的文章
.factory('AccountArticleService', function($http,BASE_URL) { 
	return {
		//获取我的文章列表
		MyArticles:function(){
			return $http.post(BASE_URL + '/member/article/list',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		}
	}
})
//我二手商品
.factory('AccountUsedService', function($http,BASE_URL) { 
	return {
		//获取我的二手商品列表
		MyUsed:function(account,token,checkkey){
			return $http.post(BASE_URL + '/market/mys',{account:account,token:token,checkkey:checkkey});
		}
	}
})
//我的活动
.factory('AccountActivityService', function($http,BASE_URL) { 
	return {
		//获取我发布的活动列表
		MyArtivity:function(){
			return $http.post(BASE_URL + '/member/active/mys',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		},
		//获取我参与的活动列表
		JoinActivity:function(){
			return $http.post(BASE_URL + '/member/active/joins',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		}
	}
})
//我的好友
.factory('FriendsService', function($http,BASE_URL) { 
	return {
		//获取我的好友列表
		MyFriends:function(account,token,checkkey){
			return $http.post(BASE_URL + '/member/friends/list',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		}
	}
})
//我的好友详情
.factory('FriendDetialService', function($http,BASE_URL) { 
	return {
		//发送好友申请
		BeMyFriend:function(id,message){
			return $http.post(BASE_URL + '/member/friends/request/add',{message:message,memberid:id,account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		}
	}
})
//搜索好友
.factory('FriendSearchService', function($http,BASE_URL) { 
	return {
		//获取搜索好友列表
		SearchFriends:function(key){
			return $http.post(BASE_URL + '/member/friends/search',{key:key});
		}
	}
})
//好友聊天对话框
.factory('FriendDialogService', function($http,BASE_URL) { 
	return {
		//获取我的文章列表
		MyArticles:function(){
			return $http.post(BASE_URL + '/member/article/list',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		},
		//获取我发布的活动列表
		MyArtivity:function(){
			return $http.post(BASE_URL + '/member/active/mys',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		},
		//获取参与我发布的用户列表
		MyArtivityUser:function(){
			return $http.post(BASE_URL + '/member/active/request/list',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		},
		//获取我参与的活动列表
		JoinActivity:function(){
			return $http.post(BASE_URL + '/member/active/joins',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		},
		//同意或者拒绝其他用户的活动申请
		ConfirmActivity:function(status,id){
			return $http.get(BASE_URL + '/member/active/request/confirm?requestid='+id+'&status='+status+'&account='+localStorage.account+'&token='+localStorage.token+'&checkkey='+localStorage.checkkey);
		},
		//好友请求确认
		FriendsConfirm:function(status,id){
			return $http.post(BASE_URL + '/member/friends/request/confirm',{requestid:id,status:status,account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		},
		//获取好友申请列表
		FriendsList:function(){
			return $http.post(BASE_URL + '/member/friends/request/list',{account:localStorage.account,token:localStorage.token,checkkey:localStorage.checkkey});
		}

	}
})
.directive('onRepeatFinishedRender', function ($timeout) {
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      if (scope.$last === true) {
        $timeout(function () {
          //这里element, 就是ng-repeat渲染的最后一个元素
          scope.$emit('ngRepeatFinished', element);
        });
      }
    }
  };
})
//结尾分号
;
