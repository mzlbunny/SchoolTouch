// Ionic Starter App
  
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js  
// 'starter.controllers' is found in controllers.js 
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform,$rootScope) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
  // 监听路由变化
  $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
    // 未登录  进入需登录页面
    //TODO 补充state name
    // console.log(localStorage.account+","+toState.name+","+localStorage.password);
    // console.log(localStorage.token+","+localStorage.checkkey);
    if (localStorage.account == undefined && (toState.name== 'tab.account')){
      event.preventDefault();
      console.log("需要登录");
      $rootScope.$broadcast('require-login', toState.name);
    }
  });

})


.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {

  //将标签栏固定在底端
  $ionicConfigProvider.tabs.position('bottom');
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

 
/*****************tab页面与子页面路由设置******************/
 .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })
 //首页路由
  .state('tab.home', {
    url: '/home',
    views: {
      'tab-home': {
        templateUrl: 'templates/tab-home.html',
        controller: 'HomeCtrl'
      }
    }
  })
 //二手市场路由
  .state('tab.usedgoods', {
      url: '/usedgoods',
      cache:false,
      views: {
        'tab-usedgoods': {
          templateUrl: 'templates/tab-usedgoods.html',
          controller: 'UsedgoodsCtrl'
        }
      }
    })
 //活动广场路由
  .state('tab.activity', {
      url: '/activity',
      cache:false,
      views: {
        'tab-activity': {
          templateUrl: 'templates/tab-activity.html',
          controller: 'ActivityCtrl' 
        }
      }
    })
 //个人中心路由
  .state('tab.account', {
    url: '/account',
    cache:false,
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })


/************************登入注册*******************************/
//1.登入
.state('login', {
  url: '/login',
  cache:false,
  templateUrl: 'templates/login.html',
  controller: 'LoginCtrl'
})
//2.注册
.state('sign', {
  url: '/sign',
  cache:false,
  templateUrl: 'templates/sign.html',
  controller: 'SignCtrl'
})

/************************二手市场*******************************/
//1.详情
.state('used-detail', {
  url: '/used-detail/:id',
  cache:false,
  templateUrl: 'templates/used-detail.html',
  controller: 'UsedDetailCtrl'
})
//2.发布
.state('used-issue', {
  url: '/used-issue',
  cache:false,
  templateUrl: 'templates/used-issue.html',
  controller: 'UsedIssueCtrl'
})

/************************活动广场*******************************/
//1.详情
.state('activity-detail', {
  url: '/activity-detail/:id',
  cache:false,
  templateUrl: 'templates/activity-detail.html',
  controller: 'ActivityDetailCtrl'
})
//2.发布
.state('activity-issue', {
  url: '/activity-issue',
  cache:false,
  templateUrl: 'templates/activity-issue.html',
  controller: 'ActivityIssueCtrl'
})
/************************个人中心*******************************/
//1.详细资料
.state('account-detail', {
  url: '/account-detail',
  cache:false,
  templateUrl: 'templates/account-detail.html',
  controller: 'AccountDetailCtrl'
})
//2.自己发布的活动列表
.state('account-activity', {
  url: '/account-activity',
  cache:false,
  templateUrl: 'templates/account-activity.html',
  controller: 'AccountActivityCtrl'
})
//3.自己发布的二手商品列表
.state('account-used', {
  url: '/account-used',
  cache:false,
  templateUrl: 'templates/account-used.html',
  controller: 'AccountUsedCtrl'
})
//4.我的文章
.state('account-article', {
  url: '/account-article',
  cache:false,
  templateUrl: 'templates/account-article.html',
  controller: 'AccountArticleCtrl'
})
/************************好友列表*******************************/
//1.好友列表
.state('friends', {
  url: '/friends',
  cache:false,
  templateUrl: 'templates/tab-friends.html',
  controller: 'FriendsCtrl'
})
//2.好友资料详情
.state('friends-detail', {
  url: '/friends-detail/:name',
  cache:false,
  templateUrl: 'templates/friends-detail.html',
  controller: 'FriendsDetailCtrl'
})
//3.好友对话框
.state('friends-dialog', {
  url: '/friends-dialog',
  cache:false,
  templateUrl: 'templates/friends-dialog.html',
  controller: 'FriendsDialogCtrl'
})
//4.对话框
.state('dialog-detail', {
  url: '/dialog-detail/:id',
  cache:false,
  templateUrl: 'templates/dialog-detail.html',
  controller: 'DialogDetailCtrl'
})
//5.查询好友
.state('friends-search', {
  url: '/friends-search',
  templateUrl: 'templates/friends-search.html',
  controller: 'FriendsSearchCtrl'
})
/************************文章详情*******************************/
//1.文章详情
.state('article-detail', {
  url: '/article-detail/:id',
  cache:false,
  templateUrl: 'templates/article-detail.html',
  controller: 'ArticleDetailCtrl'
})
//2.文章发布
.state('article-issue', {
  url: '/article-issue',
  cache:false,
  templateUrl: 'templates/article-issue.html',
  controller: 'ArticleIssueCtrl'
})
//3.通知详情
.state('news-detail', {
  url: '/news-detail/:id',
  cache:false,
  templateUrl: 'templates/news-detail.html',
  controller: 'NewsDetailCtrl'
})
/************************个人云定位*******************************/
.state('myposition', {
  url: '/myposition',
  cache:false,
  templateUrl: 'templates/my-position.html',
  controller: 'MyPositionCtrl'
})
//路由结束的分号
;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/home');

});
